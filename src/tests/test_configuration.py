from segment import Segment
from configuration import *

epsilon = 1e-6


# Test if the score function works as expected
def test_score_target_abs():
    s1 = Segment(0.0, 4.0, 10.0)
    s2 = Segment(0.0, 4.0, 10.0, 0.0, 6.0)
    segments = [s1, s2]
    assert abs(score_target_abs(segments) - 6.0) < epsilon


# Test the creation of random configurations respects some rules
def test_create_random_configuration():
    segments = create_random_configuration(60.0, 100.0, 7)
    assert len(segments) == 7
    assert segments[0].start_time < epsilon
    assert abs(segments[-1].end_time - 100.0) < epsilon


# Check the check config function
def test_check_config():
    s1 = Segment(0.0, 4.0, 10.0)
    s2 = Segment(0.0, 4.0, 10.0, 0.0, 6.0)
    segments = [s1, s2]
    assert check_config(segments, 15.0)
    assert not check_config(segments, 30.0)
