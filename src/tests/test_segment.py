from segment import Segment

epsilon = 1e-6

# Test linked to the creation of a Segment object
def test_segment_creation():
    s = Segment(1.0, 5.0, 10.0, 0.0, 10.0)
    assert abs(s.min_len - 1.0) < epsilon
    assert abs(s.max_len - 10.0) < epsilon
    assert abs(s.target_len - 5.0) < epsilon
    assert abs(s.start_time - 0.0) < epsilon
    assert abs(s.end_time - 10.0) < epsilon
    assert abs(s.cur_len - (s.end_time - s.start_time)) < epsilon
    assert abs(s.order - s.target_len) < epsilon


# Test for setting start and end time
def test_set_time():
    s = Segment()
    s.set_start_time(1.0)
    s.set_end_time(7.0)
    assert abs(s.start_time - 1.0) < epsilon
    assert abs(s.end_time - 7.0) < epsilon


# Test the tolerance function
def test_tolerance():
    s = Segment(1.0, 5.0, 10.0, 0.0, 5.0)
    # Ask for 10 but only 5 available
    assert abs(s.tolerance(10.0) - 5.0) < epsilon
    # Come back to previous state
    s.tolerance(-5.0)

    # Ask for -10 but only -4 available
    assert abs(s.tolerance(-10.0) + 4.0) < epsilon
    # Ask for 2. and it's OK
    assert abs(s.tolerance(2.0) - 2.0) < epsilon


# Test if random segments respect the min/target/max length
def test_randomize():
    s = Segment()
    s.randomize(12.0)
    assert 12.0 - s.max_len >= 0.0
    assert s.max_len - s.target_len >= 0.0
    assert abs(s.order - s.target_len) < epsilon
    assert s.target_len - s.min_len >= 0.0
    assert s.min_len > 0.0
