import random as rand

# Segment class
class Segment:
    # Initialization of a segment
    # tar_len is the target length, i.e. the best possible
    #           length for the segment ont its own
    # min / max lengths are self explanatory
    # start / end times are self explanatory
    # cur_len is the current length
    # order it the length that the segment is asked to take
    #           several actions try that value and confront it
    #           with the 'real life'
    def __init__(
        self, min_duration=0.0, target_duration=0.0, max_duration=0.0, t0=0.0, t1=0.0
    ):
        assert target_duration >= 0.0 and target_duration >= min_duration
        assert min_duration >= 0.0
        assert max_duration >= 0.0 and max_duration >= target_duration
        self.target_len = target_duration
        self.min_len = min_duration
        self.max_len = max_duration
        self.start_time = t0
        self.end_time = t1
        self.cur_len = self.end_time - self.start_time
        self.order = self.target_len

    # Define the starting time of the segment
    def set_start_time(self, time):
        assert time > 0.0
        self.start_time = time
        self.cur_len = self.end_time - self.start_time

    # Define the ending time of the segment
    def set_end_time(self, time):
        assert time > 0.0
        self.end_time = time
        self.cur_len = self.end_time - self.start_time

    # Test if a given shift from target value stays in authorized zone
    def tolerance(self, time):
        # If the segment has enough space, it can cope with the full shift required
        if self.order + time >= self.min_len and self.order + time <= self.max_len:
            self.order += time
            return time
        # The segment is too close to one of its boundaries to take the full shift
        else:
            if time < 0:
                diff = self.min_len - self.order
                self.order = self.min_len
                return diff
            elif time > 0:
                diff = self.max_len - self.order
                self.order = self.max_len
                return diff
            else:
                return 0

    # Print some info about a segment
    def print(self):
        print(
            "%.5f" % self.min_len,
            "\t",
            "%.5f" % self.target_len,
            "\t",
            "%.5f" % self.max_len,
            "\t | ",
            "%.5f" % self.cur_len,
            "\t | ",
            "%.5f" % self.start_time,
            " -> ",
            "%.5f" % self.end_time,
            end="",
        )

        epsilon = 1e-5
        # If the current length is smaller than min length (should not happen !)
        if self.cur_len + epsilon < self.min_len:
            print(" < ")
        # If the current length is larger than max length (should not happen !)
        elif self.cur_len - epsilon > self.max_len:
            print(" > ")
        else:
            print()

    # Create a random segment
    def randomize(self, duration: float):
        # 0.2s equivalent to 300bpm which should be enough
        self.max_len = rand.uniform(0.2, duration)
        # Target length is less than or equal to max length
        self.target_len = rand.uniform(0.2, self.max_len)
        self.order = self.target_len
        # Min length is less than or equal to target length
        self.min_len = rand.uniform(0.0, self.target_len)
