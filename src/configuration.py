import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from segment import Segment

# Compute the score (with respect to target length) by abs sum
def score_target_abs(segments):
    delta = 0.0
    for s in segments:
        delta += abs(s.target_len - s.cur_len)

    return delta


# Compute the score (with respect to the tempo map) by abs sum
def score_beat_abs(segments, tempo_map):
    delta = 0.0
    for s in segments:
        beat_delta = 9999.9
        for b in tempo_map:
            if beat_delta > abs(b - s.cur_len):
                beat_delta = abs(b - s.cur_len)
        delta += beat_delta

    return delta


# Print all segments
def print_config(segments, tempo_map):
    # Print segment characteristics
    for s in segments:
        s.print()

    # Compute configuration score
    print(f"Config score (target) : {score_target_abs(segments)}")
    print(f"Config score (beat) : {score_beat_abs(segments, tempo_map)}")


# Check if configuration can work
def check_config(segments, duration: float):
    assert duration > 0.0

    sum_max_len = 0.0
    sum_min_len = 0.0
    for s in segments:
        sum_max_len += s.max_len
        sum_min_len += s.min_len

    if duration < sum_min_len or duration > sum_max_len:
        return False

    return True


# Create a given number of segments with random length values
def create_random_configuration(bpm: int, duration: float, nbSegments: int):
    segments = []

    check = False
    nbTests = 0
    while not check:
        nbTests += 1
        segments = []
        # Random segments
        for i in range(nbSegments):
            s = Segment()
            s.randomize(bpm * duration / (20 * nbSegments))
            segments.append(s)

        # Check if config can work before continuing
        check = check_config(segments, duration)

    print(f"{nbTests} random configuration(s) needed to find one that could work")

    segments[0].start_time = 0.0
    segments[-1].end_time = duration

    return segments


# Init segment config
def init_config(segments, duration: float):

    # Spread deviation between cumulative length and audio length over segments as homogeneously as possible
    sum_min_len = 0.0
    sum_target_len = 0.0
    sum_max_len = 0.0
    for s in segments:
        sum_min_len += s.min_len
        sum_target_len += s.target_len
        sum_max_len += s.max_len

    delta = sum_target_len - duration
    print(f"Total min length : {sum_min_len}")
    print(f"Total target length : {sum_target_len}")
    print(f"Total max length : {sum_max_len}")
    print(f"Total audio duration : {duration}")
    print(f"\t Difference : {delta}", end=" ")
    if delta > 0:
        print("--> Time to subtract by segment : ", delta / len(segments))
    elif delta < 0:
        print("--> Time to add by segment : ", -delta / len(segments))
    print()

    tot_shift = -delta
    nbPlayers = len(segments)
    nbRounds = 0
    # 10 ms is considered simultaneous for human beings
    while abs(tot_shift) > 0.1:
        nbRounds += 1
        if nbPlayers > 0:
            avg_shift = tot_shift / nbPlayers
        else:
            print("No more segment to adjust")
            print("Dead end ! There is no solution !")
            print("\t--> Let's try something else ...")
            # Add small push
            tot_shift = -delta * 2
            # Reinitialize
            nbPlayers = len(segments)
            for s in segments:
                s.order = s.target_len
            continue
        # Ask politely each segment if it can take the shift
        for i in range(len(segments)):
            s = segments[i]
            if s.order > s.min_len and s.order < s.max_len:
                shift_accepted = s.tolerance(avg_shift)
                if abs(shift_accepted - avg_shift) > 1e-5:
                    nbPlayers -= 1
                tot_shift -= shift_accepted
            # Move end of current segment
            s.set_end_time(s.start_time + s.order)
            # Move start of next segment
            if i < len(segments) - 1:
                segments[i + 1].set_start_time(s.end_time)
            # To adjust automatically cur_time
            elif i == len(segments) - 1:
                segments[i].set_end_time(duration)
        # If last segment is out of bounds start over and ask the first
        # segments to take more than this time
        if (
            segments[-1].order > segments[-1].max_len
            or segments[-1].order < segments[-1].min_len
        ):
            # Add small push
            tot_shift = -delta * 2
            # Reinitialize
            nbPlayers = len(segments)
            for s in segments:
                s.order = s.target_len

    print(f"{nbRounds} iterations were necessary to find an approximate solution\n")


# Adjust segments according to tempo
def bpm_adjust(segments, tempo_map):

    beat_start = 1
    # For each segment we check the nearest beat that allows to stay
    # inside the given limits
    for i in range(len(segments) - 1):
        s = segments[i]
        delta = 99999.9
        s.set_end_time(s.start_time + s.order)
        for j in range(beat_start, len(tempo_map)):
            b = tempo_map[j]
            new_delta = abs(b - (s.start_time + s.order))
            if (
                delta > new_delta
                and b - s.start_time > s.min_len
                and b - s.start_time < s.max_len
            ):
                delta = new_delta
                s.set_end_time(b)
                beat_start = j

        segments[i + 1].set_start_time(segments[i].end_time)


# Display plot to show result with matplotlib
def display_solution(tempo_map, segments):
    fig, ax = plt.subplots()
    ax.set_xlim(0, tempo_map[-1])
    ax.set_ylim(-1, 1)

    # Tempo map
    for i in tempo_map:
        ax.add_patch(Rectangle((i, -1), 0.05, 2, color="black"))

    # Segments
    for s in segments:
        ax.add_patch(
            Rectangle(
                (s.start_time, -0.5),
                s.cur_len,
                1,
                facecolor="lightblue",
                edgecolor="red",
            )
        )
    plt.title(f"Score (bpm) : {score_beat_abs(segments,tempo_map)}")
    plt.tight_layout()
    plt.show()
