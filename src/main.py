#!/usr/bin/python3

import sys
from configuration import *

# Default to non verbose mode
VERBOSE = False
DISPLAY = True

# Analyze command line arguments
def analyze_arguments():
    global VERBOSE
    global DISPLAY
    # Default bpm
    bpm = 100
    duration = 200.0
    nbSegments = 10
    if len(sys.argv) > 1:
        for i in sys.argv:
            print(i[:6])
            if i == "-h" or i == "--help":
                print("Here are the possible arguments :")
                print("\t -h or --help : print this help")
                print("\t -v           : verbose mode")
                print("\t --bpm=n      : audio beats per minute")
                print("\t --time=x     : length of audio file")
                print("\t --nbseg=n    : number of segments to use")
                print("\t --nodisplay  : does not display matplotlib figures")
                exit()
            elif i == "-v":
                VERBOSE = True
            elif i[:6] == "--bpm=":
                bpm = int(i[6:])
            elif i[:7] == "--time=":
                duration = float(i[7:])
            elif i[:8] == "--nbseg=":
                nbSegments = int(i[8:])
            elif i == "--nodisplay":
                DISPLAY = False

    if VERBOSE:
        if len(sys.argv) > 1:
            print("Argument list for " + sys.argv[0] + " :")
            for i in range(1, len(sys.argv)):
                print("\t" + sys.argv[i])
        else:
            print("No argument given for " + sys.argv[0] + " :")

    return bpm, duration, nbSegments


# Create time sequence according to bpm and duration of the audio clip
def create_bpm_mapping(bpm: float, duration: float):
    assert bpm > 0.0
    assert duration > 0.0
    step = 60.0 / bpm
    nbBeats = int(duration / step)
    tempo_map = [t * step for t in range(nbBeats + 1)]

    # Add the end of the interval whether the beat hits at duration or not
    if tempo_map[-1] != duration:
        tempo_map.append(duration)
    return tempo_map


# Main routine
def main():
    # Analyze & print arguments
    bpm, duration, nbSegments = analyze_arguments()

    # Create music beat array
    tempo_map = create_bpm_mapping(bpm, duration)
    assert len(tempo_map) > 0

    # Create animation segments
    segments = create_random_configuration(bpm, duration, nbSegments)

    # Compute approximate homogeneous split of time warp
    init_config(segments, duration)
    if VERBOSE:
        print("\n=======================")
        print("Coarse config")
        print("=======================")
        print_config(segments, tempo_map)

    if DISPLAY:
        display_solution(tempo_map, segments)

    # Apply tempo map on segments
    bpm_adjust(segments, tempo_map)
    if VERBOSE:
        print("\n=======================")
        print("Fine config")
        print("=======================")
        print_config(segments, tempo_map)

    # Print tempo map
    if VERBOSE:
        print("\nTempo map")
        print(list("%.1f" % beat for beat in tempo_map))

    # Display result with matplotlib
    if DISPLAY:
        display_solution(tempo_map, segments)


if __name__ == "__main__":
    main()
